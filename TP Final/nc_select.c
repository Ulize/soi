#include <netdb.h>
#include <assert.h>
#include <netinet/ip.h>
#include <stdio.h>
#include <string.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdarg.h>
#include <errno.h>
#include <stdlib.h>

typedef struct sockaddr_in sin;
typedef struct sockaddr    sad;

static void die(char *s, ...)
{
	va_list v;

	va_start(v, s);
	vfprintf(stderr, s, v);
	fprintf(stderr, "\n");
	va_end(v);
	fprintf(stderr, " -- errno = %i (%m)\n", errno);

	fflush(stderr);
	abort();
}

int create_sock(char *host, char *port)
{
	int sock, ret;
	struct sockaddr_in addr;
	struct addrinfo *gai, hints;

	sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock < 0)
		die("socket");

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;

	/*
	 * Consultamos la información sobre la dirección que nos
	 * dieron. Podemos pasar una IP, o un nombre que será
	 * consultado a /etc/hosts o al nameserver configurado
	 */
	ret = getaddrinfo(host, port, &hints, &gai);
	if (ret)
		die("getaddrinfo (%s)", gai_strerror(ret));

	/*
	 * getaddrinfo devuelve una lista enlazada con
	 * información, tomamos el primer nodo
	 */

	addr = *(sin*)gai->ai_addr;

	freeaddrinfo(gai);

	/* Conectamos a esa dirección */
	ret = connect(sock, (sad*)&addr, sizeof addr);
	if (ret < 0)
		die("connect");

	return sock;
}

void loop(int sock)
{
	fd_set fds;
	FD_ZERO(&fds);
	FD_SET(sock, &fds);
	FD_SET(0, &fds);

	int rc;
	rc = select(sock+1, &fds, NULL, NULL, NULL);
	if (rc < 0)
		die("select");

	fprintf(stderr, "select retorna %i\n", rc);

	if (FD_ISSET(0, &fds)) {
		char buf[200];
		int n = read(0, buf, 200);
		assert(n > 0);
		// Asumo que esto no bloquea! Pero puede...
		write(sock, buf, n);

		/* Para manejar correctamente el write bloqueante */
		// rc = send(sock, buf, n, MSG_DONTWAIT);
		// if (rc == n) {
		// 	/* listo */
		// } else {
		// 	/* anotar que falta mandar
		// 	 * n-rc bytes,
		// 	 * y agregar sock a writefds  (select) */
		// }
	}

	if (FD_ISSET(sock, &fds)) {
		char buf[200];
		int n = read(sock, buf, 200);
		assert(n > 0);
		write(1, buf, n);
	}

	loop(sock);
}

int main(int argc, char **argv)
{
	int sock;

	if (argc != 3)
		abort();

	sock = create_sock(argv[1], argv[2]);

	loop(sock);

	return 0;
}