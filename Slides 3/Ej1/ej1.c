#include<unistd.h>
#include<stdlib.h>
#include<stdio.h>

void charAtATime(char *string) {
  char* ptr = string;
  setbuf(stdout, NULL);
  for (; *ptr; ptr++) {
    putc(*ptr, stdout);
  }
}

int main(){
  pid_t pid = fork();
  if (pid == 0) { // child
    charAtATime("Hello World! (child)\n");
  }
  else if (pid > 0) { // parent
    charAtATime("Hello World! (parent)\n");
  } else {
    printf("fork failed\n");
  }
  return 0;
}
