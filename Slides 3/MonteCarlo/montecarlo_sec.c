#include<stdlib.h>
#include<stdio.h>
#include<pthread.h>
#include<time.h>

#define TOTAL 10000000

void* set_point(void * arg){
  float point[2];
  for (size_t i = 0; i < TOTAL; i++) {
    point[0] = (float)rand()/RAND_MAX;
    point[1] = (float)rand()/RAND_MAX;
    float dist = (point[0]-0.5) * (point[0]-0.5) + (point[1]-0.5) * (point[1]-0.5);
    if (dist <= 0.25) {
      *((int*)arg) += 1;
    }
  }
}

int main(){
  srand((unsigned int)time(NULL));
  int r = 0, points;
  points = 0;
  pthread_t thread;
  pthread_create(&thread, NULL, set_point, (void*)&points);
  pthread_join(thread, NULL);
  printf("%d-%d\n", points, TOTAL);
  float pi = (float)(4 * points) / TOTAL;
  printf("Pi = %f\n", pi);
  return 0;
}
