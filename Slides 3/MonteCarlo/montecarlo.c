#include<stdlib.h>
#include<stdio.h>
#include<pthread.h>
#include<time.h>

#define TOTAL_PER_THREAD 100000
#define THREAD_N 10

void* set_point(void * arg){
  float point[2];
  for (size_t i = 0; i < TOTAL_PER_THREAD; i++) {
    point[0] = (float)rand()/RAND_MAX;
    point[1] = (float)rand()/RAND_MAX;
    float dist = (point[0]-0.5) * (point[0]-0.5) + (point[1]-0.5) * (point[1]-0.5);
    if (dist <= 0.25) {
      *((int*)arg) += 1;
    }
  }
}

int main(){
  srand((unsigned int)time(NULL));
  int points = 0; /* Points in the circle */
  pthread_t thread;
  for (int i = 0; i < THREAD_N; i++) {
    pthread_create(&thread, NULL, set_point, (void*)&points);
    pthread_join(thread, NULL); /* Works the same without this line */
  }
  float pi = (float)(4 * points) / (TOTAL_PER_THREAD * THREAD_N);
  printf("Pi = %f\n", pi);
  return 0;
}
