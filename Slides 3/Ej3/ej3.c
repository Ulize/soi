#include<unistd.h>
#include<stdlib.h>
#include<stdio.h>
#include<pthread.h>

void* hello_world(void * arg){
  printf("Hello world!\n");
}

int main(int argc, char** arv){
  pthread_t thread;
  pthread_create(&thread, NULL, hello_world, NULL);
  pthread_join(thread, NULL);
  return 0;
}
