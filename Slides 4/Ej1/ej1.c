#include<unistd.h>
#include<stdlib.h>
#include<stdio.h>
#include<pthread.h>
#define VISITORS 4000
#define TRIPOD_N 4

void* tripod(void * arg){
  for (int i = 0; i < VISITORS/TRIPOD_N; i++) {
    (*(int*)arg)+= 1;
  }
}

int main(int argc, char** arv){
  int visitantes = 0;
  pthread_t threads[TRIPOD_N];
  for (int i = 0; i < TRIPOD_N; i++) {
    pthread_create(&(threads[i]), NULL, tripod, (void *)&visitantes);
  }
  for (int i = 0; i < TRIPOD_N; i++) {
    pthread_join(threads[i], NULL);
  }
  printf("Visitantes: %d\n", visitantes);
  return 0;
}
