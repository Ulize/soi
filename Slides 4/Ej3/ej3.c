#include<unistd.h>
#include<stdlib.h>
#include<stdio.h>
#include<pthread.h>
#define VISITORS_N 4000
#define TRIPOD_N 2

int flag = 0;

void* tripod(void * arg){
  while (flag);
  flag = 1;
  for (int i = 0; i < VISITORS_N/TRIPOD_N; i++) {
    (*(int*)arg)+= 1;
  }
  flag = 0;
}

int main(int argc, char** arv){
  int visitors = 0;
  pthread_t threads[TRIPOD_N];
  for (int i = 0; i < TRIPOD_N; i++) {
    pthread_create(&(threads[i]), NULL, tripod, (void *)&visitors);
  }
  for (int i = 0; i < TRIPOD_N; i++) {
    pthread_join(threads[i], NULL);
  }
  printf("Visitors: %d\n", visitors);
  return 0;
}
