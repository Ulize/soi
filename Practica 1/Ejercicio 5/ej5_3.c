#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#define N_FILOSOFOS 5
#define ESPERA 5000000

pthread_mutex_t tenedor[N_FILOSOFOS];
sem_t sem;

void pensar(int i) {
  printf("Filosofo %d pensando...\n",i);
  usleep(random() % ESPERA);
}

void comer(int i) {
  printf("Filosofo %d comiendo...\n",i);
  usleep(random() % ESPERA);
}

void tomar_tenedores(int i) {
  sem_wait(&sem);
  pthread_mutex_lock(&tenedor[i]); /* Toma el tenedor a su derecha */
  pthread_mutex_lock(&tenedor[(i+1)%N_FILOSOFOS]); /* Toma el tenedor a su izquierda */
  sem_post(&sem);
}

void dejar_tenedores(int i) {
  pthread_mutex_unlock(&tenedor[i]); /* Deja el tenedor de su derecha */
  pthread_mutex_unlock(&tenedor[(i+1)%N_FILOSOFOS]); /* Deja el tenedor de su izquierda */
}

void *filosofo(void *arg) {
  int i = (int)arg;
  for (;;) { 
    tomar_tenedores(i);
    comer(i);
    dejar_tenedores(i);
    pensar(i);
  }
}
int main() {
  int i;
  pthread_t filo[N_FILOSOFOS];
  sem_init(&sem, 0, N_FILOSOFOS - 1);
  for (i=0;i<N_FILOSOFOS;i++) pthread_mutex_init(&tenedor[i], NULL);
  for (i=0;i<N_FILOSOFOS;i++) pthread_create(&filo[i], NULL, filosofo, (void *)i);
  pthread_join(filo[0], NULL);
  sem_destroy(&sem);
  return 0;
}

/*Otro filósofo piensa que tampoco tendrían el problema si todos fuesen
diestros pero sólo comiesen a lo sumo N − 1 de ellos a la vez.
Implemente esta solución y explique por qué funciona. 
Para ello va a necesitar un semáforo de Dijkstra. Puede utilizar los POSIX Semaphores. En
la cabecera semaphore.h puede encontrar los prototipos de las funciones
necesarias:
int sem_init(sem_t *sem, int pshared, unsigned int value);
int sem_destroy(sem_t *sem);
int sem_wait(sem_t *sem);
int sem_post(sem_t *sem);
int sem_getvalue(sem_t *sem, int *valp);

Funciona porque siempre va a haber un tenedor libre
*/