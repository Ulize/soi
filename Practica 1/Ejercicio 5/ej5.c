#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#define N_FILOSOFOS 5
#define ESPERA 5000000

pthread_mutex_t tenedor[N_FILOSOFOS];

void pensar(int i) {
  printf("Filosofo %d pensando...\n",i);
  usleep(random() % ESPERA);
}

void comer(int i) {
  printf("Filosofo %d comiendo...\n",i);
  usleep(random() % ESPERA);
}

void tomar_tenedores(int i) {
  if (i==1) {
    pthread_mutex_lock(&tenedor[(i+1)%N_FILOSOFOS]); /* Toma el tenedor a su izquierda */
    pthread_mutex_lock(&tenedor[i]); /* Toma el tenedor a su derecha */
  }
  else {
    pthread_mutex_lock(&tenedor[i]); /* Toma el tenedor a su derecha */
    pthread_mutex_lock(&tenedor[(i+1)%N_FILOSOFOS]); /* Toma el tenedor a su izquierda */
  }
}

void dejar_tenedores(int i) {
  pthread_mutex_unlock(&tenedor[i]); /* Deja el tenedor de su derecha */
  pthread_mutex_unlock(&tenedor[(i+1)%N_FILOSOFOS]); /* Deja el tenedor de su izquierda */
}

void *filosofo(void *arg) {
  int i = (int)arg;
  for (;;) { 
    tomar_tenedores(i);
    comer(i);
    dejar_tenedores(i);
    pensar(i);
  }
}
int main() {
  int i;
  pthread_t filo[N_FILOSOFOS];
  for (i=0;i<N_FILOSOFOS;i++) pthread_mutex_init(&tenedor[i], NULL);
  for (i=0;i<N_FILOSOFOS;i++) pthread_create(&filo[i], NULL, filosofo, (void *)i);
  pthread_join(filo[0], NULL);
  return 0;
}

/* Este programa puede terminar en deadlock. ¿En qu´e situaci´on se puede
dar?
En el caso que todos los filosofos tomen un tenedor y asi ninguno tendra 2
*/

/*Cansados de no comer los filósofos deciden pensar una solución a su problema. 
Uno razona que esto no sucedería si alguno de ellos fuese zurdo y
tome primero el tenedor de su izquierda.
Implemente esta solución y explique por qué funciona.
Si el primer filosofo esta esperando el tenedor izquierdo es que a su izquierda
le sacaron el tenedor, si ese filosofo esta esperando es porque le hicieron lo mismo
y asi hasta el filosofo a la derecha del primero que puede tomar el tenedor a su
izquierda, ya que el primer filosofo es zurdo.
Si el primer filosofo esta esperando el tenedor derecho es que a su derecha
tomaron dos tenedores y el filosofo esta comiendo.
*/