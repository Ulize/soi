#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>

sem_t tabaco, papel, fosforos, otra_vez, turn[3], elem;
int tabCant = 0, papCant = 0, fosCant = 0;

void *inspector(void* arg) {
  int c;
  for (;;) { 
    sem_wait(&elem); // espera dos elementos
    sem_wait(&elem);

    if (tabCant) {
      if (papCant) {
        tabCant = papCant = 0;
        sem_post(&turn[0]);
      }
      else {
        tabCant = fosCant = 0;
        sem_post(&turn[1]);
      }  
    }
    else {
      papCant = fosCant = 0;
      sem_post(&turn[2]);
    }
  }
}

void *listener_tab(void* arg) {
  sem_t *item = (sem_t *)arg;
  for (; ;) {
    sem_wait(item);
    tabCant++;
    sem_post(&elem);
  }
}

void *listener_pap(void* arg) {
  sem_t *item = (sem_t *)arg;
  for (; ;) {
    sem_wait(item);
    papCant++;
    sem_post(&elem);
  }
}

void *listener_fos(void* arg) {
  sem_t *item = (sem_t *)arg;
  for (; ;) {
    sem_wait(item);
    fosCant++;
    sem_post(&elem);
  }
}

void fumar(int fumador) {
  printf("Fumador %d: Puf! Puf! Puf!\n", fumador);
  sleep(1);
}

void agente() {
  for (;;) {
    int caso = random() % 3;
    sem_wait(&otra_vez);
    switch (caso) {
      case 0:
        sem_post(&tabaco);
        sem_post(&papel);
        break;
      case 1:
        sem_post(&fosforos);
        sem_post(&tabaco);
        break;
      case 2:
        sem_post(&papel);
        sem_post(&fosforos);
        break;
    }
  }
}

void *fumador1(void *arg) {
  for (;;) {
    sem_wait(&turn[0]);
    fumar(1);
    sem_post(&otra_vez);
  }
}

void *fumador2(void *arg){
  for (;;) {
    sem_wait(&turn[1]);
    fumar(2);
    sem_post(&otra_vez);
  }
}

void *fumador3(void *arg) {
  for (;;) {
    sem_wait(&turn[2]);
    fumar(3);
    sem_post(&otra_vez);
  }
}

int main() {
  pthread_t s1, s2, s3, ins, l1, l2, l3;
  sem_init(&turn[0], 0, 0);
  sem_init(&turn[1], 0, 0);
  sem_init(&turn[2], 0, 0);
  sem_init(&tabaco, 0, 0);
  sem_init(&papel, 0, 0);
  sem_init(&fosforos, 0, 0);
  sem_init(&otra_vez, 0, 1);
  pthread_create(&s1, NULL, fumador1, NULL);
  pthread_create(&s2, NULL, fumador2, NULL);
  pthread_create(&s3, NULL, fumador3, NULL);
  pthread_create(&ins, NULL, inspector, NULL);
  pthread_create(&l1, NULL, listener_tab, (void*)&tabaco);
  pthread_create(&l2, NULL, listener_pap, (void*)&papel);
  pthread_create(&l3, NULL, listener_fos, (void*)&fosforos);
  agente();
  return 0;
}

/*¿Cómo puede ocurrir un deadlock?
Si F1 toma el tabaco y F3 le toma el papel,
Si F2 toma los fósforos y F1 le toma el tabaco,
Si F3 toma papel y F2 le toma los fósforos,
*/


/*Implemente una solución y explíquela.*/