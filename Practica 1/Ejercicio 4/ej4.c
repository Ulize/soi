#include <stdio.h>
#include <pthread.h>
#define N_VISITANTES 100000000

pthread_mutex_t mutex;

int visitantes = 0;

void *molinete(void *arg) {
  int i;
  for (i=0;i<N_VISITANTES;i++) {
    pthread_mutex_lock(&mutex);
    visitantes++;
    pthread_mutex_unlock(&mutex);
  }
}

int main() {
  pthread_t m1, m2;
  pthread_create(&m1, NULL, molinete, NULL);
  pthread_create(&m2, NULL, molinete, NULL);
  pthread_join(m1, NULL);
  pthread_join(m2, NULL);
  pthread_mutex_destroy(&mutex);
  printf("Hoy hubo %d visitantes!\n", visitantes);
  return 0;
}

/*Por cada molinete entran N_VISITANTES personas, pero al ejecutar el programa 
es difícil que el resultado sea 2*N_VISITANTES. Explique a qué se debe esto.
Lo que ocurre es que ambos hilos leen y escriben en visitantes al mismo tiempo,
por lo que puede pasar es que ambos lean n, y escriban n + 1, en lugar de
escribir n + 2
*/

/*Ejecute el programa 5 veces con N_VISITANTES igual a 10. ¿El programa
dio el resultado correcto siempre? Si esto es as´ı, ¿por qué?
El programa dio el resultado correcto siempre, esto se debe a que 
el error anterior no ocurre siempre, y el programa tiene un comportamiento
no determinista*/

/*¿Cuál es el mínimo valor que podría imprimir el programa? ¿Bajo qué
circustancia?
El mínimo que puede imprimir es 2:
->p1 lee 0
->p2 se ejecuta hasta el ultimo ciclo y escribe N_VISITANTES - 1
->p1 escribe 1
->p2 lee 1
->p1 completa su funcionamiento
->p2 escribe 2
*/

/*Implemente una soluci´on utilizando un mutex.*/