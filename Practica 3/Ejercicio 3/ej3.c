/*
Escriba utilizando OpenMP una funci´on que verifique si un entero es primo (buscando
divisores entre 2 y √N)

Para la función que chequea primalidad, hagan que tome un long cosa de poder probarla con números grandes (hasta 2^63 - 1).
La función debería andar mejor o igual que una versión secuencial que corta apenas encuentra un divisor. 
Prueben la performance con números grandes, les sugiero usar primos de Mersenne y semiprimos.
*/

#include <stdio.h>
#include <omp.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "timing.h"

int esPrimoParalelo(unsigned long n) {
  int ret = 1;
  unsigned long p = sqrt(n);
  #pragma omp parallel for shared(ret)
  for (long m = 2; m <= p; m++)
  {
    if (n % m == 0) {
      ret = 0;
    }
  }
  return ret;
}

int esPrimoSecuencial(unsigned long n) {
  unsigned long p = sqrt(n);
  for (long m = 2; m <= p; m++) {
    if (n % m == 0) {
      printf("Factores: %ld %ld\n", m, n/m);
      return 0;
    }
  }
  return 1;
}

int main() {
  unsigned long n = 18446744073709551557; // numero primo
  int m;
  TIME(m = esPrimoParalelo(n), NULL);
  TIME(esPrimoSecuencial(n), NULL);
  if (m) printf("%lu es primo!\n", n);
  else printf("%lu no es primo\n", n);
  n = 4611686014132420609;  // numero semiprimo
  TIME(m = esPrimoSecuencial(n), NULL);
  TIME(esPrimoParalelo(n), NULL);
  if (m) printf("%lu es primo!\n", n);
  else printf("%lu no es primo\n", n);
  return 0;
}