#include <stdio.h>
#include <mpi.h>

void main(int argc, char **argv)
{
  int rank, m, value, size;
  MPI_Status status;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  m = rank;
  for (int i = 0; i < log(size)/log(2); m /= 2) {
    if (m % 2) {
      /* code */
    }
    
  }
  
  MPI_Bcast(&value, 1, MPI_INT, 0, MPI_COMM_WORLD);
  printf("Suma total: %d\n", value);
  MPI_Finalize();
}
