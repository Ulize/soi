#include <stdio.h>
#include <pthread.h>
#include "timing.h"
#include <stdlib.h>

#define PTHREAD_N 4
pthread_mutex_t mutex;
double sum = 0;
double *arr;
long long int sz = 5e8;

void *summ(void *arg) {
	int id = arg - NULL;
  for (int i = 0 + (sz/PTHREAD_N) * id; i < (sz/PTHREAD_N) * (id+1); i++) {
    pthread_mutex_lock(&mutex);
    sum = sum + arr[i];
    pthread_mutex_unlock(&mutex);
  }
}

int main() {
  pthread_mutex_init(&mutex, NULL);
  pthread_t threads[PTHREAD_N];
  float v;
  arr = malloc(sizeof(double) * sz);
  if (arr == NULL) perror("Error\n");
  for (int i = 0; i < sz; i++) {
    arr[i] = (double) i;
  }
  for (int i = 0; i < PTHREAD_N; i++) {
    pthread_create(&threads[i], NULL, summ, i + NULL);
  }
  

  printf("Sum: %f!\n", sum);
  printf("Expected value: %lld!\n", ((sz * (sz - 1)))/2);
  free(arr);
  return 0;
}