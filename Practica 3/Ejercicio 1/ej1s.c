#include <stdio.h>
#include "timing.h"
#include <stdlib.h>

int main() {
  long long int sz = 5e8;
  double sum = 0;
  float v;
  double *arr = malloc(sizeof(double) * sz);
  if (arr == NULL) perror("Error\n");
  for (int i = 0; i < sz; i++) {
    arr[i] = (double) i;
  }
  TIME(NULL, &v);
  for (int i = 0; i < sz; i++)
  {
  sum = sum + arr[i];
  }
  printf("Sum: %f!\n", sum);
  printf("Expected value: %lld!\n", ((sz * (sz - 1)))/2);
  free(arr);
  return 0;
}