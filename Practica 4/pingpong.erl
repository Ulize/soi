-module(pingpong).
-export([play/0, ping/0, pong/0]).

ping() ->
  receive
    {0, _} -> io:fwrite("Fin~n");
    {N, PongProcessId} -> io:fwrite("Ping: ~p ~n", [N]),
                          PongProcessId ! {N, self()},
                          ping()
end.

pong() ->
  receive
    {1, PingProcessId} -> io:fwrite("Pong: 1 ~n"),
                          PingProcessId ! {0, self()};
    {N, PingProcessId} -> io:fwrite("Pong: ~p ~n", [N]),
                          PingProcessId ! {N-1, self()},
                          pong()
end.


play()->
  PingProcessId = spawn(pingpong, ping, []),
  PongProcessId = spawn(pingpong, pong, []),
  PingProcessId ! {10, PongProcessId}.