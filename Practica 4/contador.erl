-module(contador).
-export([loop/1, start/0]).


loop(N) ->
  receive
    terminar -> ok;
    inc -> loop(N+1);
    dec -> loop(N+1);
    value -> io:fwrite("~p~n", [N]),
             loop(N)
  end.

start() -> spawn(contador, loop, [0]).
