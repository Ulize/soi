#define _GNU_SOURCE

#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <string.h>
#include <sys/epoll.h>
#include <sys/socket.h>
#include <sys/syscall.h>
#include <unistd.h>
#include <pthread.h>
#include <assert.h>
#include <stdarg.h>
#include <errno.h>
#include <stdlib.h>

typedef struct sockaddr_in sin;
typedef struct sockaddr    sad;

static void die(char *s, ...)
{
	va_list v;

	va_start(v, s);
	vfprintf(stderr, s, v);
	fprintf(stderr, "\n");
	va_end(v);
	fprintf(stderr, " -- errno = %i (%m)\n", errno);

	fflush(stderr);
	abort();
}

pid_t tid()
{
	return (pid_t)syscall(__NR_gettid) - getpid();
}

struct fdinfo {
	enum {
		LSOCK,
		CLIENT
	} type;
	int fd;
	struct sockaddr_in sin;
};

static void print_event(int fd, struct epoll_event ev)
{
	char flags_str[200];

	flags_str[0] = 0;
	flags_str[1] = 0; /* trucho */

	if (ev.events & EPOLLIN )	strcat(flags_str, "|EPOLLIN");
	if (ev.events & EPOLLOUT)	strcat(flags_str, "|EPOLLOUT");
	if (ev.events & EPOLLERR)	strcat(flags_str, "|EPOLLERR");
	if (ev.events & EPOLLHUP)	strcat(flags_str, "|EPOLLHUP");
	if (ev.events & EPOLLRDHUP)	strcat(flags_str, "|EPOLLRDHUP");
	if (ev.events & EPOLLPRI)	strcat(flags_str, "|EPOLLPRI");
	if (ev.events & EPOLLET)	strcat(flags_str, "|EPOLLET");
	if (ev.events & EPOLLONESHOT)	strcat(flags_str, "|EPOLLONESHOT");

	fprintf(stderr, "Th:%i Evento en fd %i. Flags=(%s).\n", tid(), fd, flags_str+1);
}

int start_listener(int port)
{
	struct sockaddr_in listen_addr;
	int lsock;
	int ret;

	lsock = socket(AF_INET, SOCK_STREAM, 0);
	if (lsock < 0)
		die("socket");

	int yes = 1;
	if (setsockopt(lsock, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof yes) == 1)
		die("setsockopt");

	memset(&listen_addr, 0, sizeof listen_addr);
	listen_addr.sin_family = AF_INET;
	listen_addr.sin_port = htons(port);
	listen_addr.sin_addr.s_addr = INADDR_ANY;

	ret = bind(lsock, (sad*)&listen_addr, sizeof listen_addr);
	if (ret < 0)
		die("bind");

	ret = listen(lsock, 10);
	if (ret < 0)
		die("listen");

	return lsock;
}

void accept_one(int epfd, int lsock)
{
	int cli;
	struct fdinfo *fdinfo;
	struct epoll_event ev;
	socklen_t crap = sizeof fdinfo->sin;
	int ret;

	fdinfo = malloc(sizeof *fdinfo);

	cli = accept4(lsock, (sad*)&fdinfo->sin, &crap, SOCK_NONBLOCK);
	if (cli < 0)
		die("accept");

	fprintf(stderr, "Th:%i aceptando fd %i\n", tid(), cli);

	fdinfo->fd = cli;
	fdinfo->type = CLIENT;

	ev.data.ptr = fdinfo;
	ev.events = EPOLLIN | EPOLLRDHUP | EPOLLONESHOT;

	ret = epoll_ctl(epfd, EPOLL_CTL_ADD, cli, &ev);
	if (ret < 0)
		die("epoll_ctl.2");
}

void handle_cli(struct fdinfo *fdinfo)
{
	int fd = fdinfo->fd;
	int t;
	char buf1[101], buf2[101];

	inet_ntop(AF_INET, &fdinfo->sin.sin_addr, buf1, sizeof fdinfo->sin);
	/*
	 * Leemos y hacemos echo de todo lo que haya,
	 * hasta que la lectura falle (EAGAIN)
	 */
	do {
		buf2[0] = 0;
		t = read(fd, buf2, 100);

		/* EOF */
		if (t == 0)
			return;

		/* No hay más nada por ahora */
		if (t < 0 && (errno == EAGAIN || errno == EWOULDBLOCK))
			return;

		/* Algún error */
		if (t < 0) {
			fprintf(stderr, "error en fd %i? %i\n", fd, errno);
			return;
		}

		write(fd, buf2, t);
	} while (1);
}

void kill_cli(int epfd, struct fdinfo *fdinfo)
{
	char buf[100];
	int fd = fdinfo->fd;

	inet_ntop(AF_INET, &fdinfo->sin.sin_addr, buf, sizeof fdinfo->sin);

	printf("Th:%i: Chau %s!\n", tid(), buf);

	free(fdinfo);
	epoll_ctl(epfd, EPOLL_CTL_DEL, fd, NULL);
	close(fd);
}

void handle_event(int epfd, struct epoll_event ev)
{
	struct fdinfo *fdinfo = ev.data.ptr;
	int lsock;
	int rc;

	switch (fdinfo->type) {
	case LSOCK:
		lsock = fdinfo->fd;
		accept_one(epfd, lsock);

		ev.data.ptr = fdinfo;
		ev.events = EPOLLIN | EPOLLONESHOT;
		rc = epoll_ctl(epfd, EPOLL_CTL_MOD, lsock, &ev);
		if (rc < 0)
			die("epoll_ctl.4");

		break;

	case CLIENT:
		print_event(fdinfo->fd, ev);
		if (ev.events & EPOLLHUP) {
			kill_cli(epfd, fdinfo);
			return;
		}

		if (ev.events & EPOLLRDHUP)  {
			handle_cli(fdinfo);
			kill_cli(epfd, fdinfo);
			return;
		}

		if (ev.events & EPOLLIN) {
			handle_cli(fdinfo);
		}

		struct epoll_event ev;
		ev.events = EPOLLIN | EPOLLRDHUP | EPOLLONESHOT;
		ev.data.ptr = fdinfo;
		rc = epoll_ctl(epfd, EPOLL_CTL_MOD, fdinfo->fd, &ev);
		if (rc < 0)
			die("epoll_ctl.3");

		break;
	}
}

void service(int epfd)
{
	int i, n;
	struct epoll_event evs[10];

	for (;;) {
		n = epoll_wait(epfd, evs, 10, 2000);

		if (n < 0 && errno == EINTR)
			continue;

		if (n < 0)
			die("epoll_wait");

		if (n == 0) {
			printf("Th:%i: Que aburrimiento che....\n", tid());
			continue;
		}

		for (i = 0; i < n; i++)
			handle_event(epfd, evs[i]);
	}
}

void *service_wrap(void *arg)
{
	int epfd = *(int*)arg;

	service(epfd);

	return 0;
}

int main ()
{
	int epfd = epoll_create(1);
	int lsock = start_listener(9000);
	struct epoll_event ev;
	struct fdinfo *fdinfo;
	int ret;
	int nthr = sysconf(_SC_NPROCESSORS_ONLN);
	long i;
	pthread_t *ths;

	ths = malloc(nthr * sizeof ths[1]);

	fdinfo = malloc(sizeof (struct fdinfo));
	fdinfo->type = LSOCK;
	fdinfo->fd = lsock;

	ev.data.ptr = fdinfo;
	ev.events = EPOLLIN | EPOLLONESHOT;

	ret = epoll_ctl(epfd, EPOLL_CTL_ADD, lsock, &ev);
	if (ret < 0)
		die("epoll_ctl.1");

	for (i = 0; i < nthr; i++)
		pthread_create(&ths[i], NULL, service_wrap, &epfd);

	for (i = 0; i < nthr; i++)
		pthread_join(ths[i], NULL);

	close(epfd);

	return 0;
}