#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char** argv){
  if(argc < 3){
    printf("Los argumentos no estan completos\n");
    return 0;
  }
  while (1) {
    pid_t pid = fork();
    if (pid < 0) {
      printf("Fork error\n");
      exit(EXIT_FAILURE);
    }
    else if (pid == 0) execl(argv[1], argv[1], NULL);
    else sleep(atoi(argv[2]));
  }
  return 0;
}
