#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

int main(){
  pid_t pid = fork();
  if (pid < 0) {
    printf("Fork error\n");
    exit(EXIT_FAILURE);
  }
  else if (pid == 0) printf("Child id: %d\n", getpid());
  else printf("Parent id: %d\n", getpid());
  return 0;
}
