#include <unistd.h> /* Write/Read/lseek/fork */
#include <stdio.h> /* prints */
#include <stdlib.h> /* STD error output*/
#include <sys/wait.h> /* wait */
#include <string.h> /* strcmp */

char **dyn_scan() {
  int args = 1;
  char **comm = malloc(sizeof(char*) * args);
  for (int i = 0; i < args; i++) {
    int j = 0;
    char c;
    comm[i] = malloc(sizeof(char) * 10);
    for (int flag = 1; flag; j++) {
      c = getchar();
      if (c == ' ') {
        if (j % 10 == 0) comm[i] = realloc(comm[i], sizeof(comm[i]) + sizeof(char));
        args++;
        comm = realloc(comm, sizeof(char*) * args);
        flag--;
        comm[i][j] = '\0';
      }
      else if (c == '\n') {
        if (j % 10 == 0) comm[i] = realloc(comm[i], sizeof(comm[i]) + sizeof(char));
        flag--;
        comm[i][j] = '\0';
      }
      else {
        if (j % 10 == 0) comm[i] = realloc(comm[i], sizeof(comm[i]) + sizeof(char) * 10);
        comm[i][j] = c;
      }
    }
  }
  return comm;
}

int main() {
  int wstatus;
  char **comm;
  while (1) {
    printf(">> ");
    comm = dyn_scan();
    if (strcmp(comm[0], "exit") == 0) exit(EXIT_SUCCESS);
    else {
      pid_t pid = fork();
      if (pid < 0) {
        printf("Fork error\n");
        exit(EXIT_FAILURE);
      }
      else if (pid == 0) execvp(comm[0], comm);
      else wait(&wstatus);
    }
    for (int i = 0; i < sizeof(comm)/sizeof(char*); i++) {
      free(comm[i]);
    }
    free(comm);
  }
  return 0;
}