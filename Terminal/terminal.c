#include <unistd.h> /* Write/Read/lseek/fork */
#include <stdio.h> /* prints */
#include <stdlib.h> /* STD error output*/
#include <sys/wait.h> /* wait */
#include <string.h> /* strcmp */

int main() {
  int wstatus;
  char binaryPath[20];
  while (1) {
    printf(">> ");
    scanf("%s", binaryPath);
    if (strcmp(binaryPath, "exit") == 0) exit(EXIT_SUCCESS);
    else {
      pid_t pid = fork();
      if (pid < 0) {
        printf("Fork error\n");
        exit(EXIT_FAILURE);
      }
      else if (pid == 0) execl(binaryPath, binaryPath, NULL);
      else wait(&wstatus);
    }
  }
  return 0;
}