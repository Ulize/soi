#include "ej10.h"

void cond_init(Cond *condition) {
  sem_init(&condition->sem, 0, 0);
  condition->durmN = 0;
  pthread_mutex_init(&condition->mutex, NULL);
}

void cond_wait(Cond *condition, pthread_mutex_t *lock) {
  pthread_mutex_lock(&condition->mutex);
  condition->durmN++;
  pthread_mutex_unlock(&condition->mutex);
  pthread_mutex_unlock(lock);
  sem_wait(&condition->sem);
  pthread_mutex_lock(&condition->mutex);
  condition->durmN--;
  pthread_mutex_unlock(&condition->mutex);
  pthread_mutex_lock(lock);
}

void cond_signal(Cond *condition) {
  pthread_mutex_lock(&condition->mutex);
  int val;
  sem_getvalue(&condition->sem, &val); 
  if (val == 0) {
    pthread_mutex_unlock(&condition->mutex);
    return;
  }
  sem_post(&condition->sem);
  pthread_mutex_unlock(&condition->mutex);
}

void cond_broadcast(Cond *condition) {
  pthread_mutex_lock(&condition->mutex);
  for (int i = 0; i < condition->durmN; i++) {
    sem_post(&condition->sem);
  }
  pthread_mutex_unlock(&condition->mutex);
}

void cond_destroy(Cond *condition) {
  sem_destroy(&condition->sem);
  pthread_mutex_destroy(&condition->mutex);
}
