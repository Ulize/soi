#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

typedef struct _Channel {
  int *v;
  sem_t writable, readable;
} Channel;

void channel_init(Channel *c) {
  sem_init(&c->writable, 0, 1);
  sem_init(&c->readable, 0, 0);
  return;
}

void chan_write(Channel *c, int *v) {
  sem_wait(&c->writable);
  c->v = v;
  sem_post(&c->readable);
  return;
}

int *chan_read(Channel *c) {
  sem_wait(&c->readable);
  int *ret = c->v;
  sem_post(&c->writable);
  return ret;
}

#define M 5
#define N 5

/*
 * El buffer guarda punteros a enteros, los
 * productores los consiguen con malloc() y los
 * consumidores los liberan con free()
 */

Channel canal;

void *prod_f(void *arg) {
	int id = arg - (void*)0;
	while (1) {
		sleep(random() % 3);

		int *p = malloc(sizeof *p);
		*p = random() % 100;
		printf("Productor %d: produje %p->%d\n", id, p, *p);
		chan_write(&canal, p);
	}
	return NULL;
}

void *cons_f(void *arg) {
	int id = arg - (void*)0;
	while (1) {
		sleep(random() % 3);

		int *p = chan_read(&canal);
		printf("Consumidor %d: obtuve %p->%d\n", id, p, *p);
		free(p);
	}
	return NULL;
}

int main() {
	pthread_t productores[M], consumidores[N];
  channel_init(&canal);
  int i;
	for (i = 0; i < M; i++)
		pthread_create(&productores[i], NULL, prod_f, i + (void*)0);

	for (i = 0; i < N; i++)
		pthread_create(&consumidores[i], NULL, cons_f, i + (void*)0);

	pthread_join(productores[0], NULL); /* Espera para siempre */
	return 0;
}
