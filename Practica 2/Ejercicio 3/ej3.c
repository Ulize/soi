#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#define M 5
#define N 5
#define ARRLEN 10240

int n = 0;
pthread_cond_t zero_lectors;
pthread_mutex_t cant, read_write;

int arr[ARRLEN];
void * escritor(void *arg) {
  int i;
  int num = arg - (void*)0;
  while (1) {
    sleep(random() % 3);
    pthread_mutex_lock(&read_write);
    pthread_mutex_lock(&cant);
    while (n != 0) {
      pthread_mutex_unlock(&read_write);
      pthread_cond_wait(&zero_lectors, &cant);
      pthread_mutex_lock(&read_write);
    };
    pthread_mutex_unlock(&cant);
    printf("Escritor %d escribiendo\n", num);
    for (i = 0; i < ARRLEN; i++)
      arr[i] = num;
  }
  return NULL;
}

void * lector(void *arg) {
  int v, i;
  int num = arg - (void*)0;
  while (1) {
    sleep(random() % 3);
    pthread_mutex_lock(&read_write);
    pthread_mutex_lock(&cant);
    n++;
    pthread_mutex_unlock(&cant);
    pthread_mutex_unlock(&read_write);
    v = arr[0];
    for (i = 1; i < ARRLEN; i++) {
      if (arr[i] != v) break;
    }
    if (i < ARRLEN) printf("Lector %d, error de lectura\n", num);
    else printf("Lector %d, dato %d\n", num, v);
    pthread_mutex_lock(&cant);
    n--;
    if (n==0) pthread_cond_broadcast(&zero_lectors);
    pthread_mutex_unlock(&cant);
  }
  return NULL;
}

int main() {
  pthread_t lectores[M], escritores[N];
  int i;
  for (i = 0; i < M; i++)
    pthread_create(&lectores[i], NULL, lector, i + (void*)0);
  for (i = 0; i < N; i++)
    pthread_create(&escritores[i], NULL, escritor, i + (void*)0);
  pthread_join(lectores[0], NULL);/* Espera para siempre */
  return 0;
}
