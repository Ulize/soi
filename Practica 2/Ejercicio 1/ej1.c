#include <stdio.h>
#include <pthread.h>
#define N_VISITANTES 100000
#define N_MOLINETES 16

int nums[N_MOLINETES] = {0};
int bools[N_MOLINETES] = {0};
int visitantes = 0;
int max = 0;

void lock(int id) {
  /* Calcula el número de turno */
  asm("mfence");
  bools[id] = 1;
  nums[id] = ++max;
  bools[id] = 0;
  asm("mfence");
  /* Compara con todos los hilos */
  for (int j = 0; j < N_MOLINETES; j++) {
    /* Si el hilo j está calculando su número, espera a que termine */
    asm("mfence");
    while (bools[j]){ /* busy waiting */ }
    /* Si el hilo j tiene más prioridad, espera a que ponga su número a cero */
    /* j tiene más prioridad si su número de turno es más bajo que el de i, */
    /* o bien si es el mismo número y además j es menor que i*/
    asm("mfence");
    while ((nums[j] != 0) &&
          ((nums[j] < nums[id]) || ((nums[j] == nums[id]) && (j < id))) ) { /* busy waiting */ }
  }
}

void unlock(int id) {
  nums[id] = 0;
}

void *molinete(void *arg) {
  int id = (int)arg;
  for (int i = 0; i < N_VISITANTES; i++) {
    lock(id);
    visitantes++;
    unlock(id);
  }
}

int main() {
  pthread_t m[N_MOLINETES];
  for (int i = 0; i < N_MOLINETES; i++) {
    pthread_create(&m[i], NULL, molinete, (void *)i);
  }
  for (int i = 0; i < N_MOLINETES; i++) {
    pthread_join(m[i], NULL);
  }
  printf("Hoy hubo %d visitantes!\n", visitantes);
  return 0;
}